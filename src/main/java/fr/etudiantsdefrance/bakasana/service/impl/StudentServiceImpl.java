package fr.etudiantsdefrance.bakasana.service.impl;

import fr.etudiantsdefrance.bakasana.dao.StudentDao;
import fr.etudiantsdefrance.bakasana.model.Student;
import fr.etudiantsdefrance.bakasana.model.StudentDto;
import fr.etudiantsdefrance.bakasana.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@Service(value = "studentService")
public class StudentServiceImpl implements UserDetailsService, StudentService {
	
	@Autowired
	private StudentDao studentDao;

	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;

	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Student user = studentDao.findByEmail(email);
		if(user == null){
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), getAuthority());
	}

	private List<SimpleGrantedAuthority> getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}

	public List<Student> findAll() {
		List<Student> list = new ArrayList<>();
		studentDao.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public void delete(long id) {
		studentDao.deleteById(id);
	}

	@Override
	public Student findOne(String username) {
		return studentDao.findByEmail(username);
	}

    @Override
    public Optional<Student> findById(Long id) {
        return studentDao.findById(id);
    }

	@Override
    public Student save(StudentDto user) {
		Student newUser = Student.builder()
			.email(user.getEmail())
			.password(user.getPassword())
			.build();

        return studentDao.save(newUser);
    }
}
