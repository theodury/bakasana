package fr.etudiantsdefrance.bakasana.service;

import fr.etudiantsdefrance.bakasana.model.Student;
import fr.etudiantsdefrance.bakasana.model.StudentDto;

import java.util.List;
import java.util.Optional;

public interface StudentService {

    Student save(StudentDto user);
    List<Student> findAll();
    void delete(long id);
    Student findOne(String username);

    Optional<Student> findById(Long id);
}
