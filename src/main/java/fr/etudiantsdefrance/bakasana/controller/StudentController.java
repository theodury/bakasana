package fr.etudiantsdefrance.bakasana.controller;

import fr.etudiantsdefrance.bakasana.model.Student;
import fr.etudiantsdefrance.bakasana.model.StudentDto;
import fr.etudiantsdefrance.bakasana.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;

    @RequestMapping(value="/users", method = RequestMethod.GET)
    public List<Student> listUser(){
        return studentService.findAll();
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public Optional<Student> getOne(@PathVariable(value = "id") Long id){
        return studentService.findById(id);
    }

    @RequestMapping(value="/signup", method = RequestMethod.POST)
    public Student saveUser(@RequestBody StudentDto user){
        return studentService.save(user);
    }

}
