package fr.etudiantsdefrance.bakasana.controller;

import fr.etudiantsdefrance.bakasana.config.JwtTokenUtil;
import fr.etudiantsdefrance.bakasana.model.AuthToken;
import fr.etudiantsdefrance.bakasana.model.LoginUser;
import fr.etudiantsdefrance.bakasana.model.Student;
import fr.etudiantsdefrance.bakasana.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/token")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/generate-token", method = RequestMethod.POST)
    public ResponseEntity<?> register(@RequestBody LoginUser loginUser) throws AuthenticationException {

        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getEmail(),
                        loginUser.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final Student student = studentService.findOne(loginUser.getEmail());
        final String token = jwtTokenUtil.generateToken(student);
        return ResponseEntity.ok(new AuthToken(token));
    }

}
