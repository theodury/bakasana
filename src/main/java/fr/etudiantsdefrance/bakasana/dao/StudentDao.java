package fr.etudiantsdefrance.bakasana.dao;

import fr.etudiantsdefrance.bakasana.model.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentDao extends CrudRepository<Student, Long> {
    Student findByEmail(String email);
}
