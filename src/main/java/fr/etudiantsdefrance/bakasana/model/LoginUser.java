package fr.etudiantsdefrance.bakasana.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class LoginUser {

    private String email;
    private String password;

}
