package fr.etudiantsdefrance.bakasana.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student {

    @Id
    @Column(name="student_id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    @Column(name="student_email")
    private String email;
    @Column(name="pass")
    @JsonIgnore
    private String password;

}
