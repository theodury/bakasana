package fr.etudiantsdefrance.bakasana.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class StudentDto {

    private String email;
    private String password;

}
